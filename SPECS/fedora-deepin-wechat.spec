Name:       fedora-deepin-wechat
Version:    3.3.0
Release:    115
Summary:    Deepin Wechat
Group:      Applications/Internet                         
License:    GPL
URL:        N/A
source0:    fedora-deepin-wechat-%{version}.tar.gz

%description
Wechat based on deepin-wine5 packed in RPM on fedora 32 x86-64 distros

%prep
%setup -q

%build
%global debug_package %{nil}

%install
cd %_builddir/fedora-deepin-wechat-3.3.0/
cp -r ./ %{buildroot}/

%files
%defattr (-,root,root)
/usr/share/*
/opt/*

%changelog
* Mon Jul 12 2021 - 3.3.0.115-1 update to version 3.3.0
- 支持朋友圈
